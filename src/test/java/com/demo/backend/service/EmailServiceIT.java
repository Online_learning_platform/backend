package com.demo.backend.service;

import com.demo.backend.service.api.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Locale;

@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        "spring.mail.host=smtp.yandex.ru",
        "spring.mail.port=465",
        "spring.mail.username=olp.dpi.2022@yandex.ru",
        "spring.mail.password=awzlqztwefhncjtj",
        "spring.mail.properties.mail.smtp.auth=true",
        "spring.mail.properties.mail.smtp.starttls.enable=true",
        "spring.mail.properties.mail.smtp.ssl.enable=true"
})
public class EmailServiceIT {
    private static final String RECIPIENT = "orderkworinaa@gmail.com"; // might be any real mail

    private static final Locale LOCALE = Locale.ENGLISH;

    @Autowired
    private EmailService emailService;


    @Test
    public void shouldSendSimpleMessage() {

        emailService.sendSimpleMessage(RECIPIENT, "Первое письмо", "Ураааа!!\nSDasdasd");

        Assertions.assertNotNull(1);
    }

    @Test
    public void shouldSendHtmlMessage() {

        emailService.sendHtmlMessage(RECIPIENT, "Первое письмо", "Ураааа!!<br>Dasdasd");

        Assertions.assertNotNull(1);
    }


}

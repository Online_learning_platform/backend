package com.demo.backend.security;


import com.demo.backend.model.Administrator;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.Student;
import com.demo.backend.model.SubAdministrator;
import com.demo.backend.model.User;
import com.demo.backend.model.misc.Role;
import com.demo.backend.security.jwt.JwtUserFactory;
import com.demo.backend.service.api.AdministratorService;
import com.demo.backend.service.api.LecturerService;
import com.demo.backend.service.api.StudentService;
import com.demo.backend.service.api.SudAdministratorService;
import com.demo.backend.service.api.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class JwtUserDetailsService implements UserDetailsService {


    public final AdministratorService administratorService;

    public final SudAdministratorService sudAdministratorService;

    private final LecturerService lecturerService;

    private final StudentService studentService;

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User existingUser = userService.getByEmailOrPhone(login);
        return JwtUserFactory.create(existingUser);
    }

    public UserDetails loadById(String userId, Role role) throws UsernameNotFoundException {
        User user = userService.getById(userId);
        String contextId = getContextId(user, role);
        return JwtUserFactory.create(user, role, contextId);
    }

    private String getContextId(User user, Role role) {
        switch (role) {
            case ROLE_STUDENT:
                return studentService.getByUser(user)
                        .map(Student::getId)
                        .orElseThrow(() -> new AccessDeniedException("Can not access this source"));
            case ROLE_LECTURER:
                return lecturerService.getByUser(user)
                        .map(Lecturer::getId)
                        .orElseThrow(() -> new AccessDeniedException("Can not access this source"));
            case ROLE_ADMINISTRATOR:
                return administratorService.getByUser(user)
                        .map(Administrator::getId)
                        .orElseThrow(() -> new AccessDeniedException("Can not access this source"));
            case ROLE_SUB_ADMINISTRATOR:
                return sudAdministratorService.getByUser(user)
                        .map(SubAdministrator::getId)
                        .orElseThrow(() -> new AccessDeniedException("Can not access this source"));
            default:
                throw new AccessDeniedException("Can not access this source");
        }

    }
}

package com.demo.backend.controller;

import com.demo.backend.dto.AnswerTaskResponse;
import com.demo.backend.dto.TaskRequest;
import com.demo.backend.dto.lecturer.LecturerRoomShortResponse;
import com.demo.backend.dto.room.LectureMaterialRequest;
import com.demo.backend.dto.room.RoomLecturerResponse;
import com.demo.backend.dto.task.TaskResponse;
import com.demo.backend.model.Student;
import com.demo.backend.service.api.RoomService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Lecturer")
@AllArgsConstructor
@RestController
@RequestMapping("lecturer")
public class LecturerController {

    @Autowired
    private RoomService roomService;

    //видит список комнат
    @PreAuthorize("hasRole('ROLE_LECTURER')")
    @GetMapping("/get/rooms")
    public ResponseEntity<LecturerRoomShortResponse> getAllSubjectInRoom() {
        return ResponseEntity.ok(roomService.getLecturerListRoom());
    }

    //сделать задание
    @PreAuthorize("hasRole('ROLE_LECTURER')")
    @PostMapping("/create/task")
    public ResponseEntity<TaskResponse> createTaskInRoom(TaskRequest taskRequest) {
        return ResponseEntity.ok(roomService.createTaskInRoom(taskRequest));
    }

    //открытие комнаты
    @PreAuthorize("hasRole('ROLE_LECTURER')")
    @PostMapping("/open-lecturer-room/{roomId}")
    public ResponseEntity<RoomLecturerResponse> openLecturerRoom(@PathVariable("roomId") String roomId) {
        return ResponseEntity.ok(roomService.openLecturerRoom(roomId));
    }

    //открытие комнаты/добавление лекции
    @PreAuthorize("hasRole('ROLE_LECTURER')")
    @PostMapping("/add-material/{roomId}")
    public ResponseEntity<RoomLecturerResponse> addLectureMaterials(@PathVariable("roomId") String roomId,
                                                                    LectureMaterialRequest lectureMaterialRequest) {
        return ResponseEntity.ok(roomService.addLectureMaterials(roomId, lectureMaterialRequest));
    }

    // открытие комнаты/задание/Список раобот на проверку

    // открытие комнаты/задание/Отправить на даработку с коментарием
    @PreAuthorize("hasRole('ROLE_LECTURER')")
    @PostMapping("/refactorTask/{answerId}")
    public ResponseEntity<AnswerTaskResponse> sendTaskToRefactor(@PathVariable("answerId") String answerId,
                                                                 Student student) {
        return ResponseEntity.ok(roomService.sendTaskToRefactor(answerId, student));
    }

    // открытие комнаты/задание/Зачесть и поставить оценку
    @PreAuthorize("hasRole('ROLE_LECTURER')")
    @PostMapping("/passTask/{answerId}")
    public ResponseEntity<AnswerTaskResponse> passTask(@PathVariable("answerId") String answerId,
                                                       Student student,
                                                       Integer grade) {
        return ResponseEntity.ok(roomService.passTask(answerId, student, grade));
    }
    //групповое задание / задание по времени
    //Уведомления


}

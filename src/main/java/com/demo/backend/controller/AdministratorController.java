package com.demo.backend.controller;

import com.demo.backend.dto.SubAdministratorRequest;
import com.demo.backend.dto.SubAdministratorResponse;
import com.demo.backend.dto.UserResponse;
import com.demo.backend.model.misc.Role;
import com.demo.backend.service.api.AdministratorService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Set;

@Tag(name = "Administrator")
@AllArgsConstructor
@RestController
@RequestMapping("administrator")
public class AdministratorController {

    @Autowired
    private AdministratorService administratorService;

    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    @PostMapping("/create/sub-admin")
    public ResponseEntity<SubAdministratorResponse> createSubAdministrator(@Valid @RequestBody SubAdministratorRequest subAdministratorRequest) {
        return ResponseEntity.ok(administratorService.createSubAdministrator(subAdministratorRequest));
    }

    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    @PostMapping("/delete/sub-admin")
    public ResponseEntity<SubAdministratorResponse> deleteSubAdministrator(@RequestParam(defaultValue = "id") String subAdminId) {
        return ResponseEntity.ok(administratorService.deleteSubAdministrator(subAdminId));
    }

    // назначение сущесвующих ползователей
    //Todo
    @PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
    @PostMapping("/add-role")
    public ResponseEntity<UserResponse> updateUserRoles(String email, Set<Role> rolesToAdd, Set<Role> rolesToRemove) {
        return ResponseEntity.ok(administratorService.updateUserRoles(email, rolesToAdd, rolesToRemove));
    }
}

package com.demo.backend.controller;

import com.demo.backend.dto.GroupRequest;
import com.demo.backend.dto.GroupResponse;
import com.demo.backend.dto.StudentRequest;
import com.demo.backend.dto.StudentResponse;
import com.demo.backend.dto.SubjectRequest;
import com.demo.backend.dto.SubjectResponse;
import com.demo.backend.dto.lecturer.LecturerRequest;
import com.demo.backend.dto.lecturer.LecturerResponse;
import com.demo.backend.dto.room.RoomRequest;
import com.demo.backend.dto.room.RoomShortResponse;
import com.demo.backend.service.api.GroupService;
import com.demo.backend.service.api.LecturerService;
import com.demo.backend.service.api.RoomService;
import com.demo.backend.service.api.StudentService;
import com.demo.backend.service.api.SubjectService;
import com.demo.backend.service.api.SudAdministratorService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "SubAdministrator")
@AllArgsConstructor
@RestController
@RequestMapping("sub-administrator")
public class SubAdministratorController {

    @Autowired
    private SudAdministratorService subAdministratorService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private RoomService roomService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private LecturerService lecturerService;

    //Вернуть список студентов
    @PreAuthorize("hasRole('ROLE_SUB_ADMINISTRATOR')")
    @GetMapping("/get/students")
    public ResponseEntity<List<StudentResponse>> getAllStudent() {
        return ResponseEntity.ok(studentService.getAllStudents());
    }

    //Вернуть список групп
    @PreAuthorize("hasRole('ROLE_SUB_ADMINISTRATOR')")
    @GetMapping("/get/groups")
    public ResponseEntity<List<GroupResponse>> getAllGroups() {
        return ResponseEntity.ok(groupService.getAllGroupResponse());
    }

    //Вернуть список преподавателей
    @PreAuthorize("hasRole('ROLE_SUB_ADMINISTRATOR')")
    @GetMapping("/get/lecturer")
    public ResponseEntity<List<LecturerResponse>> getAllLecturer() {
        return ResponseEntity.ok(lecturerService.getLecturerResponse());
    }

    //Вернуть список предметов
    @PreAuthorize("hasRole('ROLE_SUB_ADMINISTRATOR')")
    @GetMapping("/get/subject")
    public ResponseEntity<List<SubjectResponse>> getAllSubject() {
        return ResponseEntity.ok(subjectService.getAll());
    }

    //Вернуть список комнат
    @PreAuthorize("hasRole('ROLE_SUB_ADMINISTRATOR')")
    @GetMapping("/get/rooms")
    public ResponseEntity<List<RoomShortResponse>> getAllRoom() {
        return ResponseEntity.ok(roomService.getAllByCurrentUser());
    }

    //Создать студетоты
    @PreAuthorize("hasRole('ROLE_SUB_ADMINISTRATOR')")
    @PostMapping("/create/student")
    public ResponseEntity<StudentResponse> createStudent(@Valid @RequestBody StudentRequest studentRequest) {
        return ResponseEntity.ok(subAdministratorService.createStudent(studentRequest));
    }
    
    //Сгруппировать в группы
    @PreAuthorize("hasRole('ROLE_SUB_ADMINISTRATOR')")
    @PostMapping("/create/group")
    public ResponseEntity<GroupResponse> createGroup(@Valid @RequestBody GroupRequest GroupRequest) {
        return ResponseEntity.ok(groupService.createGroup(GroupRequest));
    }

    //Создать преподавателей
    @PreAuthorize("hasRole('ROLE_SUB_ADMINISTRATOR')")
    @PostMapping("/create/lecturer")
    public ResponseEntity<LecturerResponse> createLecturer(@Valid @RequestBody LecturerRequest lecturerRequest) {
        return ResponseEntity.ok(subAdministratorService.createLecturer(lecturerRequest));
    }


    //Создайте предмета
    @PreAuthorize("hasRole('ROLE_SUB_ADMINISTRATOR')")
    @PostMapping("/create/subject")
    public ResponseEntity<SubjectResponse> createSubject(@Valid @RequestBody SubjectRequest subjectRequest) {
        return ResponseEntity.ok(subjectService.createSubject(subjectRequest));
    }
    // их наполнение


    //Создание комнаты и добавлениее  добавлениее в ние предмета
    @PreAuthorize("hasRole('ROLE_SUB_ADMINISTRATOR')")
    @PostMapping("/create/{subjectId}/room}")
    public ResponseEntity<RoomShortResponse> createRoom(@PathVariable("subjectId") String subjectId, @Valid @RequestBody RoomRequest roomRequest) {
        return ResponseEntity.ok(roomService.createRoom(roomRequest, subjectId));
    }

    //И все это желательно экспортировать из csv ???

}

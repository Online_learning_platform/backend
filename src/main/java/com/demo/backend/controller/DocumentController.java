package com.demo.backend.controller;


import com.demo.backend.model.misc.DocumentOrigin;
import com.demo.backend.service.api.DocumentService;
import com.demo.backend.service.api.IPFSService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Tag(name = "Document")
@AllArgsConstructor
@RestController
@RequestMapping("document")
public class DocumentController {

    private final IPFSService ipfsService;

    private final DocumentService documentService;


    @GetMapping("create/directory")//Todo create, but not now
    public String createDirectory(@RequestParam String fileName) {
        return ipfsService.createDirectory(fileName);
    }

    @PostMapping(value = "create/{hash}/file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String createDirectoryAndAddFiles(@PathVariable("hash") String hash,
                                             @RequestParam("file") MultipartFile file) {
        return ipfsService.createDirectoryAndAddFiles(hash, file);
    }


    @PreAuthorize("hasRole('ROLE_STUDENT') or hasRole('ROLE_LECTURER') or hasRole('ROLE_ADMINISTRATOR') or hasRole('ROLE_SUB_ADMINISTRATOR')")
    @PostMapping(value = "upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String uploadFile(@RequestParam(defaultValue = "file") MultipartFile file,
                             @RequestParam DocumentOrigin origin) {
        return documentService.uploadFile(file, origin);
    }

    @GetMapping("file/{hash}")
    public ResponseEntity<byte[]> getFile(@PathVariable("hash") String hash) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-type", MediaType.ALL_VALUE);
        byte[] bytes = ipfsService.loadFile(hash);
        return ResponseEntity.status(HttpStatus.OK).headers(headers).body(Base64Utils.encode(bytes));
    }

}

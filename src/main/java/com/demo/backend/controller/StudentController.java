package com.demo.backend.controller;


import com.demo.backend.dto.TaskAndAnswerResponse;
import com.demo.backend.dto.room.RoomShortResponse;
import com.demo.backend.dto.room.RoomStudentResponse;
import com.demo.backend.service.api.RoomService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "Student")
@AllArgsConstructor
@RestController
@RequestMapping("student")
public class StudentController {

    @Autowired
    private RoomService roomService;

    //видит список комнат
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    @GetMapping("/get/rooms")
    public ResponseEntity<List<RoomShortResponse>> getAllRoom() {
        return ResponseEntity.ok(roomService.getAllByCurrentUser());
    }

    //открытие комнаты
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    @GetMapping("/get/{roomId}")
    public ResponseEntity<RoomStudentResponse> openStudentRoom(@PathVariable("roomId") String roomId) {
        return ResponseEntity.ok(roomService.openStudentRoom(roomId));
    }

    //открытие комнате/задания
    //Todo open task by type  Может быть хранить типы в словаре
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    @GetMapping("/get/{roomId}/{taskId}")
    public ResponseEntity<TaskAndAnswerResponse> openTask(@PathVariable("roomId") String roomId,
                                                          @PathVariable("taskId") String taskId) {
        return ResponseEntity.ok(roomService.openTask(roomId, taskId));
    }

    //открытие комнате/задания/прикрепить файл
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    @GetMapping("/get/{answerTaskId}/{documentId}")
    public ResponseEntity<TaskAndAnswerResponse> sendAnswer(@PathVariable("answerTaskId") String answerTaskId,
                                                            @PathVariable("documentId") String documentId) {
        roomService.sendAnswer(answerTaskId, documentId);
        return null;//ResponseEntity.ok();
        // todo
    }

    //открытие комнаты/задание/чат //отдельный контреллер

    //Уведомления //отдельный контреллер

}

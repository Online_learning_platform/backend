package com.demo.backend.service;

import com.demo.backend.dto.lecturer.LecturerResponse;
import com.demo.backend.exception.ResourceNotFoundException;
import com.demo.backend.mapper.LecturerMapperService;
import com.demo.backend.mapper.mixin.NameMixin;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.User;
import com.demo.backend.repository.LecturerRepository;
import com.demo.backend.service.api.LecturerService;
import com.demo.backend.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LecturerServiceImpl implements LecturerService, NameMixin {

    @Autowired
    private UserService userService;

    @Autowired
    private LecturerRepository lecturerRepository;

    @Autowired
    private LecturerMapperService lecturerMapperService;

    @Override
    public Lecturer getById(String id) {
        return lecturerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Lecturer not found"));
    }

    @Override
    public List<LecturerResponse> getLecturerResponse() {
        return lecturerMapperService.toListLecturerResponse(getAll());
    }

    @Override
    public List<Lecturer> getAll() {
        return lecturerRepository.findAll();
    }

    @Override
    public List<Lecturer> findLecturerByEmail(List<String> ids) {
        List<User> users = userService.getByEmails(ids).
                orElseThrow(() -> new ResourceNotFoundException("Student not found"));

        return lecturerRepository.findByUserIn(users)
                .orElseThrow(() -> new ResourceNotFoundException("Student not found"));
    }

    @Override
    public Lecturer getCurrentUserLecturer() {
        return lecturerRepository.findById(userService.getCurrentUserContextId())
                .filter(lecturer -> {
                    User currentUser = userService.getCurrentUser();
                    return lecturer.getUser().getId().equals(currentUser.getId());
                }).orElseThrow(() -> new ResourceNotFoundException("Can not find lecturer"));
    }

    @Override
    public Optional<Lecturer> getByUser(User user) {
        return lecturerRepository.findByUser(user);
    }

    @Override
    public Lecturer save(Lecturer lecturer) {
        User lecturerUser = lecturer.getUser();
        lecturer.setUser(lecturerUser);
        return lecturerRepository.save(lecturer);
    }


}
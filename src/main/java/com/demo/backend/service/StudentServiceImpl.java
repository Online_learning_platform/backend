package com.demo.backend.service;

import com.demo.backend.dto.StudentResponse;
import com.demo.backend.exception.ResourceNotFoundException;
import com.demo.backend.mapper.StudentMapperService;
import com.demo.backend.model.Student;
import com.demo.backend.model.User;
import com.demo.backend.repository.StudentRepository;
import com.demo.backend.service.api.StudentService;
import com.demo.backend.service.api.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private UserService userService;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentMapperService studentMapperService;


    @Override
    public Student getById(String id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Student not found"));
    }

    @Override
    public List<StudentResponse> getAllStudents() {
        List<Student> students = studentRepository.findAll();
        List<StudentResponse> studentResponses = studentMapperService.toStudentsResponse(students);
        return studentResponses;
    }


    @Override
    public List<Student> getAll() {
        return studentRepository.findAll();
    }


    @Override
    public List<Student> findStudentByEmail(List<String> ids) {
        List<User> users = userService.getByEmails(ids).
                orElseThrow(() -> new ResourceNotFoundException("Student not found"));

        return studentRepository.findByUserIn(users)
                .orElseThrow(() -> new ResourceNotFoundException("Student not found"));
    }


    @Override
    public List<Student> findByListId(List<String> ids) {
        return studentRepository.findByIdIn(ids)
                .orElseThrow(() -> new ResourceNotFoundException("Student not found"));
    }


    @Override
    public Student getCurrentUserStudent() {
        return studentRepository.findById(userService.getCurrentUserContextId())
                .filter(student -> {
                    User currentUser = userService.getCurrentUser();
                    return student.getUser().getId().equals(currentUser.getId());
                }).orElseThrow(() -> new ResourceNotFoundException("Can not find student"));
    }

    @Override
    public Optional<Student> getByUser(User user) {
        return studentRepository.findByUser(user);
    }

    @Override
    public Student save(Student student) {
        User studentUser = student.getUser();
        student.setUser(studentUser);
        return studentRepository.save(student);
    }

}


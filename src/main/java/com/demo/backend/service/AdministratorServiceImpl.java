package com.demo.backend.service;

import com.demo.backend.dto.SubAdministratorRequest;
import com.demo.backend.dto.SubAdministratorResponse;
import com.demo.backend.dto.UserResponse;
import com.demo.backend.mapper.SubAdministratorMapperService;
import com.demo.backend.mapper.UserMapper;
import com.demo.backend.model.Administrator;
import com.demo.backend.model.SubAdministrator;
import com.demo.backend.model.User;
import com.demo.backend.model.misc.Role;
import com.demo.backend.repository.AdministratorRepository;
import com.demo.backend.repository.SubAdministratorRepository;
import com.demo.backend.service.api.AdministratorService;
import com.demo.backend.service.api.EmailService;
import com.demo.backend.service.api.UserService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class AdministratorServiceImpl implements AdministratorService {

    @Autowired
    private SubAdministratorMapperService subAdministratorMapperService;

    @Autowired
    private SubAdministratorRepository subAdministratorRepository;


    @Autowired
    private AdministratorRepository administratorRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public SubAdministratorResponse createSubAdministrator(SubAdministratorRequest subAdministratorRequest) {
        //todo проверку почты??
        //todo SubAdministratorResponse

        SubAdministrator subAdministrator = subAdministratorMapperService.toModel(subAdministratorRequest);

        RandomStringGenerator pwdGenerator = new RandomStringGenerator.Builder().withinRange(30, 125)
                .build();

        String generate = pwdGenerator.generate(20);

        User user = subAdministrator.getUser();
        user.setRoles(Collections.singleton(Role.ROLE_SUB_ADMINISTRATOR));
        user.setPassword(passwordEncoder.encode(generate));
        User saveUser = userService.save(user);
        subAdministrator.setUser(saveUser);

        SubAdministrator saveSubAdministrator = subAdministratorRepository.save(subAdministrator);
        emailService.sendSimpleMessage(user.getEmail(), "Добро пожаловать", String.format(" ваш Логин: %s \n Пароль: %s", saveUser.getEmail(), generate));
        log.info("Fixture Administrator with id '{}' successfully created", saveSubAdministrator.getId());
        //Todo send email
        return null;
    }

    @Override
    public SubAdministratorResponse deleteSubAdministrator(String subAdministratorId) {

        subAdministratorRepository.deleteById(subAdministratorId);

        return null;
    }

    @Override
    public UserResponse updateUserRoles(String email, Set<Role> rolesToAdd, Set<Role> rolesToRemove) {
        User saveUser = userService.updateUserRoles(email, rolesToAdd, rolesToRemove);
        return userMapper.fromStudentToUserResponse(saveUser);
    }

    @Override
    public Optional<Administrator> getByUser(@NonNull User user) {
        return administratorRepository.findByUser(user);
    }

}

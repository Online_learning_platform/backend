package com.demo.backend.service;

import com.demo.backend.exception.ResourceNotFoundException;
import com.demo.backend.model.Document;
import com.demo.backend.model.misc.DocumentOrigin;
import com.demo.backend.repository.DocumentRepository;
import com.demo.backend.service.api.DocumentService;
import com.demo.backend.service.api.IPFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private IPFSService ipfsService;
    @Autowired
    private DocumentRepository documentRepository;

    @Override
    public Document getById(String documentId) {
        return findByIdInternal(documentId);
    }


    @Override
    public List<Document> getByIds(List<String> ids) {
        return documentRepository.findAllByIdIn(ids);
    }


    /*@Override
    public Resource downloadById(String documentId) {
        Document document = findByIdInternal(documentId);
        return ipfsService.downloadFile(documentId);
    }*/

    @Override
    public String uploadFile(MultipartFile file, DocumentOrigin origin) {
        String hashFileId = ipfsService.saveFile(file);

        createDocument(file.getName(), origin, hashFileId, file.getSize());

        return hashFileId;
    }

    @Override
    public Document createDocument(String fileName, DocumentOrigin documentOrigin, String hashFileId, long size) {
        Document document = new Document();
        document.setFileName(fileName);
        //document.setFileType(FileType.fromFileName(fileName));
        document.setDocumentOrigin(documentOrigin);
        document.setSize(size);
        document.setHashFileId(hashFileId);

        Document savedDocument = documentRepository.save(document);

        return savedDocument;
    }

    @Override
    public Document createDocument(String fileName, DocumentOrigin documentOrigin, String hashFileId) {
        Document document = new Document();
        document.setFileName(fileName);
        //document.setFileType(FileType.fromFileName(fileName));
        document.setDocumentOrigin(documentOrigin);
        //document.setSize(fileContent.length);
        document.setHashFileId(hashFileId);

        Document savedDocument = documentRepository.save(document);

        return savedDocument;
    }

    private Document findByIdInternal(String documentId) {
        return documentRepository.findById(documentId)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Document not found by id = '%s'", documentId)));//Todo Exception
    }


    private Document findByIdInternal(String documentId, DocumentOrigin documentOrigin) {
        return documentRepository.findByIdAndDocumentOrigin(documentId, documentOrigin)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Document not found by id = '%s'", documentId)));//Todo Exception
    }

}

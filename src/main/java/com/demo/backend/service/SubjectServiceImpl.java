package com.demo.backend.service;

import com.demo.backend.dto.SubjectRequest;
import com.demo.backend.dto.SubjectResponse;
import com.demo.backend.exception.ResourceNotFoundException;
import com.demo.backend.mapper.SubjectMapperService;
import com.demo.backend.model.Subject;
import com.demo.backend.repository.SubjectRepository;
import com.demo.backend.service.api.SubjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private SubjectMapperService subjectMapperService;


    @Override
    public List<SubjectResponse> getAll() {
        return subjectMapperService.toSubjectResponse(findAll());
    }

    @Override
    public List<Subject> findAll() {
        return subjectRepository.findAll();
    }

    @Override
    public SubjectResponse createSubject(SubjectRequest subjectRequest) {
        Subject subject = new Subject();

        subject.setName(subjectRequest.getName());
        subject.setDescription(subjectRequest.getDescription());

        Subject saveSubject = subjectRepository.save(subject);
        log.info("Subject with id '{}' successfully created", saveSubject.getId());

        return subjectMapperService.toSubjectResponse(saveSubject);

    }

    @Override
    public Subject findById(String id) {
        return subjectRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Subject not found by id '%s'", id));
    }

}

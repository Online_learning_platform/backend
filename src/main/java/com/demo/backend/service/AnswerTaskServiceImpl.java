package com.demo.backend.service;

import com.demo.backend.exception.ResourceNotFoundException;
import com.demo.backend.model.AnswerTask;
import com.demo.backend.model.Document;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.Student;
import com.demo.backend.model.Task;
import com.demo.backend.model.misc.TaskStatus;
import com.demo.backend.repository.AnswerTaskRepository;
import com.demo.backend.service.api.AnswerTaskService;
import com.demo.backend.service.api.DocumentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AnswerTaskServiceImpl implements AnswerTaskService {

    @Autowired
    private AnswerTaskRepository answerTaskRepository;

    @Autowired
    private DocumentService documentService;

    @Override
    public List<AnswerTask> createAnswerTask(Task task, List<Student> students) {

        List<AnswerTask> answerTasks = new ArrayList<>();

        students.forEach(student -> {
            AnswerTask answerTask = new AnswerTask();
            answerTask.setStudentId(student);

            AnswerTask saveAnswerTask = answerTaskRepository.save(answerTask);
            log.info("AnswerTask created by id '%s'", saveAnswerTask.getId());
            answerTasks.add(saveAnswerTask);
        });


        return answerTasks;
    }

    @Override
    public AnswerTask sendTaskToRefactor(String idTask, Student student, Lecturer lecturer) {
        AnswerTask answerTask = findByIdAndStudentId(idTask, student);

        answerTask.setTaskStatus(TaskStatus.STATUS_IN_REFACTOR);
        answerTask.setLecturerId(lecturer);

        AnswerTask saveAnswerTask = answerTaskRepository.save(answerTask);
        log.info("AnswerTask send to refactor by id '%s'", saveAnswerTask.getId());
        return saveAnswerTask;
    }


    @Override
    public AnswerTask passTask(String idTask, Student student, Integer grade, Lecturer lecturer) {
        AnswerTask answerTask = findByIdAndStudentId(idTask, student);

        answerTask.setTaskStatus(TaskStatus.STATUS_REQUIRED);
        answerTask.setLecturerId(lecturer);
        answerTask.setGrade(grade);

        AnswerTask saveAnswerTask = answerTaskRepository.save(answerTask);
        log.info("AnswerTask passed by id '%s'", saveAnswerTask.getId());
        return saveAnswerTask;
    }


    @Override
    public AnswerTask sendAnswerTask(String id, String documentId) {
        AnswerTask answerTask = findById(id);

        Document document = documentService.getById(documentId);

        answerTask.setAnswerTaskDocuments(List.of(document));
        answerTask.setTaskStatus(TaskStatus.STATUS_IN_PROGRESS);

        AnswerTask saveAnswerTask = answerTaskRepository.save(answerTask);
        log.info("Status task AnswerTask changed by id '%s'", id);
        return saveAnswerTask;
    }

    @Override
    public AnswerTask findById(String id) {
        return answerTaskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("AnswerTask not found"));
    }

    @Override
    public AnswerTask findByIdAndStudentId(String id, Student student) {
        return answerTaskRepository.findByIdAndStudentId(id, student)
                .orElseThrow(() -> new ResourceNotFoundException("AnswerTask not found"));
    }

}

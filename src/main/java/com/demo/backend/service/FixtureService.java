package com.demo.backend.service;

import com.demo.backend.model.Administrator;
import com.demo.backend.model.AnswerTask;
import com.demo.backend.model.Document;
import com.demo.backend.model.Group;
import com.demo.backend.model.LectureMaterial;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.Room;
import com.demo.backend.model.Student;
import com.demo.backend.model.SubAdministrator;
import com.demo.backend.model.Subject;
import com.demo.backend.model.Task;
import com.demo.backend.model.User;
import com.demo.backend.model.misc.DocumentOrigin;
import com.demo.backend.model.misc.Role;
import com.demo.backend.model.misc.TaskType;
import com.demo.backend.repository.AdministratorRepository;
import com.demo.backend.repository.AnswerTaskRepository;
import com.demo.backend.repository.GroupRepository;
import com.demo.backend.repository.LecturerRepository;
import com.demo.backend.repository.RoomRepository;
import com.demo.backend.repository.StudentRepository;
import com.demo.backend.repository.SubAdministratorRepository;
import com.demo.backend.repository.SubjectRepository;
import com.demo.backend.repository.TaskRepository;
import com.demo.backend.repository.UserRepository;
import com.demo.backend.service.api.DocumentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
@Service
public class FixtureService {


    @Autowired
    private AdministratorRepository administratorRepository;

    @Autowired
    private SubAdministratorRepository sudAdministratorRepository;

    @Autowired
    private LecturerRepository lecturerRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private GroupRepository groupRepository;


    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private AnswerTaskRepository answerTaskRepository;


    @Autowired
    private DocumentService documentService;


    @Value("${fixture.bootstrap.enable:false}")
    private Boolean bootstrapEnabled;


    @PostConstruct
    public void run() {
        if (bootstrapEnabled) {
            createAll();
        }
    }

    private void createAll() {
        User administratorUser = createUser("613f749ed9fceec4d1f6f47b", "admin@afanasiev.com",
                "+49", "661234561", "Admin", "Admin", "отчество", "123", Set.of(Role.ROLE_ADMINISTRATOR));

        Administrator administrator = createAdministrator("100f749ed0fceec4d1f6f47b", administratorUser);

        User subAdministratorUser = createUser("613f749ed0fceec4d1f6f47b", "subadmin@afanasiev.com",
                "+49", "661234561", "Admin", "DPI", "отчество", "123", Set.of(Role.ROLE_SUB_ADMINISTRATOR));

        SubAdministrator subAdministrator = createSubAdministrator("405f749ed9fceec0d7f6f57b", subAdministratorUser);

        User studentUser1 = createUser("613f858ed9fceec4d1f6f47b", "Anton@Afanasiev.com", "+49", "661234561", "Anton", "Afanasiev", "отчество", "DG7Uj6xp26FjFVyW", Set.of(Role.ROLE_STUDENT));
        User studentUser2 = createUser("613f858ed9fceec4d1f6f47c", "Ilya@Marinichev.com", "+49", "625712349", "Ilya", "Marinichev", "отчество", "cVHdJL4hGZ4yMGZD", Set.of(Role.ROLE_STUDENT));

        User lecturerUser1 = createUser("613f85f7da7e9127cd0d9725", "lecturer1@diplom.com", "+49", "661234562", "Jonathan", "Fischer", "отчество", "5UWXq5urus5Vz6BR", Set.of(Role.ROLE_LECTURER));
        User lecturerUser2 = createUser("613f85f7da7e9127cd0d9726", "lecturer2@diplom.com", "+49", "661234563", "Luis", "Schneider", "отчество", "mBazA7NLuEPHcf4A", Set.of(Role.ROLE_LECTURER));
        User lecturerUser3 = createUser("613f85f7da7e9127cd0d9727", "lecturer3@diplom.com", "+49", "954539108", "Elias", "Schmidt", "отчество", "6j54SPTh7LJBTjsN", Set.of(Role.ROLE_LECTURER));
        User lecturerUser4 = createUser("613f85f7da7e9127cd0d9728", "lecturer4@diplom.com", "+49", "508583695", "Finn", "Müller", "отчество", "b8MVd5xkhMhtETzc", Set.of(Role.ROLE_LECTURER));

        Task lab1 = createTask("605f86a2e4c4273987ae3eac", "lab1", TaskType.TYPE_LAB);
        Task lab2 = createTask("605f86a2e4c4273978ae3eac", "lab2", TaskType.TYPE_LAB);

        Task lab1_1 = createTask("605f87a2e4c4273744ae3eac", "lab1", TaskType.TYPE_LAB);
        Task lab2_1 = createTask("605f87a2e4c4273974ae3eac", "lab2", TaskType.TYPE_LAB);


        Lecturer lecturer1 = createLecturer("615f861f8b1317633733bda1", lecturerUser1);
        Lecturer lecturer2 = createLecturer("615f862f8b1317633737bda2", lecturerUser2);
        Lecturer lecturer3 = createLecturer("615f863f8b1717633734bda3", lecturerUser3);
        Lecturer lecturer4 = createLecturer("615f864f8b1017633725bda4", lecturerUser4);

        Student student1 = createStudent(studentUser1, "614f86a2e4c4273919ae3eac");
        Student student2 = createStudent(studentUser2, "614f86a2e4c4273715ae3ead");

        Group group = createGroup("900f86a2e4c4579975ae3ebc", "ИС-18б", List.of(student1, student2));

        AnswerTask answerTask1 = createAnswerTask("605f86a2e4c4273715ae3ead", student1);
        AnswerTask answerTask2 = createAnswerTask("605f74a2e4c4273895ae3ead", student1);
        AnswerTask answerTask3 = createAnswerTask("605f78a2e4c4983715ae3ead", student2);

        Subject subject1 = createSubject("600f871f8b1317633775bda0", "ТЗИ", "ТЗИ");
        Subject subject2 = createSubject("600f871f8b1317633774bda1", "ООП", "ООП");

        Room room1 = createRoom("605f86a2e4c4273715ae3ead", "2018", subject1, List.of(lecturer1, lecturer2), List.of(group), Map.of(group.getId(), List.of(lab1_1.getId())),
                Map.of(group.getId(),
                        Map.of(lab1_1.getId(), List.of(answerTask1.getId()))));

        Room room2 = createRoom("607f86a2e4c4273715ae7ead", "2018", subject2, List.of(lecturer3, lecturer4), List.of(group), Map.of(group.getId(), List.of(lab1.getId())),
                Map.of(group.getId(), Map.of(lab1.getId(), List.of(answerTask2.getId(),
                        answerTask3.getId()))));


        Document document = documentService.createDocument("test", DocumentOrigin.LECTURE, "QmVNZyJVrU4c2zFmNF3wxtTuQTVaAoL4K8Knqs5ksa4Wx9");
        Document document1 = documentService.createDocument("test", DocumentOrigin.LECTURE, "QmVNZyJVrU4c2zFmNF3wxtTuQTVaAoL4K8Knqs5ksa4Wx9");

        LectureMaterial lectureMaterial = createLectureMaterial("Лекция 1", "Lorem Ipsum is simply dummy text of the printing \nand typesetting industry.\n" +
                "          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\n" +
                "          It has survived not only five centuries,\n but also the leap into electronic typesetting, remaining essentially unchanged.", document);
        LectureMaterial lectureMaterial1 = createLectureMaterial("Лекция 2", "Lorem Ipsum is simply dummy text of the printing \nand typesetting industry.\n" +
                "          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\n" +
                "          It has survived not only five centuries,\n but also the leap into electronic typesetting, remaining essentially unchanged.", document1);
        room1.setLectureMaterials(List.of(lectureMaterial, lectureMaterial1));
        roomRepository.save(room1);
    }

    private LectureMaterial createLectureMaterial(String name, String description, Document document) {
        LectureMaterial lectureMaterial = new LectureMaterial();

        lectureMaterial.setName(name);
        lectureMaterial.setDescription(description);
        lectureMaterial.setLectureDocuments(List.of(document));

        return lectureMaterial;
    }

    private Room createRoom(String roomId, String name, Subject subject, List<Lecturer> lecturers, List<Group> groups, Map<String, List<String>> listTask, Map<String, Map<String, List<String>>> listTasksAndAnswers) {
        return roomRepository.findById(roomId)
                .orElseGet(() -> {
                    Room room = new Room();
                    room.setId(roomId);
                    room.setName(name);
                    room.setGroups(groups);
                    room.setSubject(subject);
                    room.setListTasks(listTask);
                    room.setLecturers(lecturers);
                    room.setListTasksAndAnswers(listTasksAndAnswers);

                    Room saveRoom = roomRepository.save(room);
                    log.info("Fixture Room with id '{}' successfully created", saveRoom.getId());
                    return saveRoom;
                });
    }

    private AnswerTask createAnswerTask(String answerTaskId, Student studentId) {
        return answerTaskRepository.findById(answerTaskId)
                .orElseGet(() -> {
                    AnswerTask answerTask = new AnswerTask();
                    answerTask.setId(answerTaskId);
                    answerTask.setStudentId(studentId);

                    AnswerTask saveAnswerTask = answerTaskRepository.save(answerTask);
                    log.info("Fixture AnswerTask with id '{}' successfully created", saveAnswerTask.getId());
                    return saveAnswerTask;
                });
    }

    private Task createTask(String taskId, String name, TaskType taskType) {
        return taskRepository.findById(taskId)
                .orElseGet(() -> {
                    Task task = new Task();
                    task.setId(taskId);
                    task.setName(name);
                    task.setTaskType(taskType);

                    Task saveTask = taskRepository.save(task);
                    log.info("Fixture Task with id '{}' and name '{}' successfully created", saveTask.getId(), saveTask.getName());
                    return saveTask;
                });
    }


    private Subject createSubject(String subjectId, String name, String description) {
        return subjectRepository.findById(subjectId)
                .orElseGet(() -> {
                    Subject subject = new Subject();
                    subject.setId(subjectId);
                    subject.setName(name);
                    subject.setDescription(description);

                    Subject saveSubject = subjectRepository.save(subject);
                    log.info("Fixture Subject with id '{}' and name '{}' successfully created", saveSubject.getId(), saveSubject.getName());
                    return saveSubject;
                });
    }


    private Group createGroup(String groupId, String name, List<Student> students) {
        return groupRepository.findById(groupId)
                .orElseGet(() -> {
                    Group group = new Group();
                    group.setId(groupId);
                    group.setName(name);
                    group.setStudents(students);


                    Group saveGroup = groupRepository.save(group);
                    log.info("Fixture Group with id '{}' successfully created", saveGroup.getId());
                    return saveGroup;
                });

    }


    private Administrator createAdministrator(String administratorId, User user) {
        return administratorRepository.findById(administratorId)
                .orElseGet(() -> {
                    Administrator administrator = new Administrator();
                    administrator.setId(administratorId);
                    administrator.setUser(user);

                    Administrator savedAdministrator = administratorRepository.save(administrator);
                    log.info("Fixture Administrator with id '{}' successfully created", savedAdministrator.getId());
                    return savedAdministrator;
                });
    }


    private SubAdministrator createSubAdministrator(String subAdministratorId, User user) {
        return sudAdministratorRepository.findById(subAdministratorId)
                .orElseGet(() -> {
                    SubAdministrator administrator = new SubAdministrator();
                    administrator.setId(subAdministratorId);
                    administrator.setUser(user);

                    SubAdministrator savedSubAdministrator = sudAdministratorRepository.save(administrator);
                    log.info("Fixture sub administrator with id '{}' successfully created", savedSubAdministrator.getId());
                    return savedSubAdministrator;
                });
    }


    private Lecturer createLecturer(String lecturerId, User user) {
        return lecturerRepository.findById(lecturerId)
                .orElseGet(() -> {
                    Lecturer lecturer = new Lecturer();
                    lecturer.setId(lecturerId);
                    lecturer.setUser(user);
                    lecturer.setCreatedAt(LocalDateTime.now());


                    Lecturer savedLecturer = lecturerRepository.save(lecturer);
                    log.info("Fixture lecturer with id '{}' successfully created", savedLecturer.getId());
                    return savedLecturer;
                });
    }


    private Student createStudent(User user, String id) {
        return studentRepository.findById(id)
                .orElseGet(() -> {
                    Student student = new Student();
                    student.setId(id);
                    student.setUser(user);
                    student.setCreatedAt(LocalDateTime.now());


                    Student savedStudent = studentRepository.save(student);
                    log.info("Fixture student with id '{}' successfully created", savedStudent.getId());
                    return savedStudent;
                });
    }

    private User createUser(String id,
                            String email,
                            String dialCode,
                            String phoneNumber,
                            String firstName,
                            String lastName,
                            String surName,
                            String password,
                            Set<Role> roles) {
        return userRepository.findById(id)
                .orElseGet(() -> {
                    User user = new User();
                    user.setId(id);
                    user.setEmail(email);
                    user.setPassword(passwordEncoder.encode(password));
                    user.setActive(true);
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    user.setSurName(surName);
                    user.setRoles(roles);
                    user.setDialCode(dialCode);
                    user.setPhoneNumber(phoneNumber);
                    user.setBirthDate(LocalDate.now());

                    User savedUser = userRepository.save(user);
                    log.info("Fixture user with email '{}' successfully created", savedUser.getEmail());
                    return savedUser;
                });
    }


}

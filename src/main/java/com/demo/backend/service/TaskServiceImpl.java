package com.demo.backend.service;

import com.demo.backend.dto.TaskRequest;
import com.demo.backend.exception.ResourceNotFoundException;
import com.demo.backend.model.Document;
import com.demo.backend.model.Task;
import com.demo.backend.repository.TaskRepository;
import com.demo.backend.service.api.DocumentService;
import com.demo.backend.service.api.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private DocumentService documentService;

    @Override
    public Task createTask(TaskRequest taskRequest) {
        Task task = new Task();

        task.setName(taskRequest.getName());
        task.setDescription(taskRequest.getDescription());

        List<String> documentIds = taskRequest.getDocumentIds();
        List<Document> documents = new ArrayList<>();
        documentIds.forEach((id) -> {
            Document document = documentService.getById(id);
            documents.add(document);
        });
        task.setTaskDocuments(documents);

        Task saveTask = taskRepository.save(task);

        return saveTask;
    }


    @Override
    public Task findById(String id) {
        return taskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Task not found"));
    }

    @Override
    public List<Task> getAllByIds(List<String> ids) {
        return taskRepository.findAllByIdIn(ids)
                .orElseThrow(() -> new ResourceNotFoundException("Tasks not found"));
    }

}

package com.demo.backend.service;

import com.demo.backend.config.IPFSConfig;
import com.demo.backend.service.api.IPFSService;
import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.multihash.Multihash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

@Service
public class IPFSServiceImpl implements IPFSService {

    @Autowired
    private IPFSConfig ipfsConfig;

    @Override
    public String saveFile(String filePath) {
        try {
            IPFS ipfs = ipfsConfig.ipfs;
            File _file = new File(filePath);

            NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(_file);
            MerkleNode response = ipfs.add(file).get(0);
            System.out.println("Hash (base 58): " + response.hash.toBase58());
            return response.hash.toBase58();
        } catch (IOException ex) {
            throw new RuntimeException("Error whilst communicating with the IPFS node", ex);
        }
    }

    @Override
    public String createDirectory(String nameDirectory) {
        List<MerkleNode> response;
        try {
            IPFS ipfs = ipfsConfig.ipfs;

            NamedStreamable.DirWrapper directory = new NamedStreamable.DirWrapper(nameDirectory, Arrays.asList());
            response = ipfs.add(directory);
            response.forEach(merkleNode -> {
                System.out.println("Hash (base 58): " + merkleNode.name.get() + " - " + merkleNode.hash.toBase58());

            });
            return response.get(0).hash.toBase58();
        } catch (IOException ex) {
            throw new RuntimeException("Error whilst communicating with the IPFS node", ex);
        }

    }

    @Override
    public String createDirectoryAndAddFiles(String hashDirectory, MultipartFile multipartFiles) {
        try {
            IPFS ipfs = ipfsConfig.ipfs;

            // List<File> files = new List<File>;
            InputStream initialStream = multipartFiles.getInputStream();
            byte[] buffer = new byte[initialStream.available()];
            initialStream.read(buffer);

            File targetFile = new File("test.txt");

            try (OutputStream outStream = new FileOutputStream(targetFile)) {
                outStream.write(buffer);
            }

/*
            files.forEach( file -> {
                try {
                    bytearrays.add(file.getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });*/


            NamedStreamable.FileWrapper file1 = new NamedStreamable.FileWrapper(targetFile);

            NamedStreamable.DirWrapper directory = new NamedStreamable.DirWrapper("QmTtiYiqJneRUH1NPrinfipdfEaKVdYvHEisMa3LrNZGEY", Arrays.asList(file1));
            List<MerkleNode> response = ipfs.add(directory);
            response.forEach(merkleNode ->
                    System.out.println("Hash (base 58): " + merkleNode.name.get() + " - " + merkleNode.hash.toBase58()));
            return response.get(response.size() - 1).hash.toBase58();
        } catch (IOException ex) {
            throw new RuntimeException("Error whilst communicating with the IPFS node", ex);
        }
    }


    @Override
    public String saveFile(MultipartFile file) {


        try {
            IPFS ipfs = ipfsConfig.ipfs;

            NamedStreamable.InputStreamWrapper is = new NamedStreamable.InputStreamWrapper(file.getName(), file.getInputStream());
            MerkleNode response = ipfs.add(is).get(0);
            System.out.println("Hash (base 58): " + response.name.get() + " - " + response.hash.toBase58());
            return response.hash.toBase58();

        } catch (IOException ex) {
            throw new RuntimeException("Error whilst communicating with the IPFS node", ex);
        }

    }

    @Override
    public byte[] loadFile(String hash) {
        try {

            IPFS ipfs = ipfsConfig.ipfs;
            Multihash filePointer = Multihash.fromBase58(hash);

            return ipfs.cat(filePointer);
        } catch (IOException ex) {
            throw new RuntimeException("Error whilst communicating with the IPFS node", ex);
        }
    }

}

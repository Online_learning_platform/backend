package com.demo.backend.service;

import com.demo.backend.dto.StudentRequest;
import com.demo.backend.dto.StudentResponse;
import com.demo.backend.dto.lecturer.LecturerRequest;
import com.demo.backend.dto.lecturer.LecturerResponse;
import com.demo.backend.exception.ResourceNotFoundException;
import com.demo.backend.mapper.StudentMapperService;
import com.demo.backend.mapper.SubAdministratorMapperService;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.Student;
import com.demo.backend.model.SubAdministrator;
import com.demo.backend.model.User;
import com.demo.backend.model.misc.Role;
import com.demo.backend.repository.LecturerRepository;
import com.demo.backend.repository.StudentRepository;
import com.demo.backend.repository.SubAdministratorRepository;
import com.demo.backend.service.api.SudAdministratorService;
import com.demo.backend.service.api.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Slf4j
@Service
public class SubAdministratorServiceImpl implements SudAdministratorService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private LecturerRepository lecturerRepository;

    @Autowired
    private SubAdministratorRepository subAdministratorRepository;

    @Autowired
    private SubAdministratorMapperService subAdministratorMapperService;

    @Autowired
    private StudentMapperService studentMapperService;

    @Autowired
    private UserService userService;

    @Override
    public StudentResponse createStudent(StudentRequest studentRequest) {
        User currentUser = userService.getCurrentUser();

        Student subStudent = studentMapperService.toModel(studentRequest);

        User user = subStudent.getUser();
        user.setRoles(Collections.singleton(Role.ROLE_STUDENT));
        user.setPassword("123");

        User saveUser = userService.save(subStudent.getUser());

        Student saveStudent = studentRepository.save(subStudent);
        log.info("Student with id '{}' successfully created", saveStudent.getId());
        //Todo send email
        return studentMapperService.toStudentResponse(saveStudent);
    }

    @Override
    public LecturerResponse createLecturer(LecturerRequest lecturerRequest) {
        User currentUser = userService.getCurrentUser();

        Lecturer subLecturer = subAdministratorMapperService.toModelLecturer(lecturerRequest);

        User user = subLecturer.getUser();
        user.setRoles(Collections.singleton(Role.ROLE_STUDENT));

        User saveUser = userService.save(user);
        subLecturer.setUser(saveUser);

        Lecturer saveLecturer = lecturerRepository.save(subLecturer);
        log.info("Lecturer with id '{}' successfully created", saveLecturer.getId());
        //Todo send email
        return subAdministratorMapperService.toLecturerResponse(saveLecturer);
    }


    @Override
    public SubAdministrator getById(String id) {
        return subAdministratorRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Administrator not found by id = '%s'", id));
    }

    @Override
    public Optional<SubAdministrator> getByUser(User user) {
        return subAdministratorRepository.findByUser(user);
    }


}

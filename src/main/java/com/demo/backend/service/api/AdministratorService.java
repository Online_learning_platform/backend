package com.demo.backend.service.api;

import com.demo.backend.dto.SubAdministratorRequest;
import com.demo.backend.dto.SubAdministratorResponse;
import com.demo.backend.dto.UserResponse;
import com.demo.backend.model.Administrator;
import com.demo.backend.model.User;
import com.demo.backend.model.misc.Role;

import java.util.Optional;
import java.util.Set;

public interface AdministratorService {

    SubAdministratorResponse createSubAdministrator(SubAdministratorRequest subAdministratorRequest);

    SubAdministratorResponse deleteSubAdministrator(String subAdministratorId);

    UserResponse updateUserRoles(String email, Set<Role> rolesToAdd, Set<Role> rolesToRemove);

    Optional<Administrator> getByUser(User user);

}

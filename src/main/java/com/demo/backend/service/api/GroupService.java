package com.demo.backend.service.api;

import com.demo.backend.dto.GroupRequest;
import com.demo.backend.dto.GroupResponse;
import com.demo.backend.model.Group;
import com.demo.backend.model.Student;

import java.util.List;

public interface GroupService {
    List<GroupResponse> getAllGroupResponse();

    GroupResponse createGroup(GroupRequest groupRequest);

    Group findByStudentListContaining(Student student);

    List<Group> findAll();

    Group findById(String id);

    List<Group> findByListId(List<String> ids);

    Group findByStudent(Student student);
}

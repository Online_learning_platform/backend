package com.demo.backend.service.api;

import com.demo.backend.dto.AnswerTaskResponse;
import com.demo.backend.dto.TaskAndAnswerResponse;
import com.demo.backend.dto.TaskRequest;
import com.demo.backend.dto.lecturer.LecturerRoomShortResponse;
import com.demo.backend.dto.room.LectureMaterialRequest;
import com.demo.backend.dto.room.RoomLecturerResponse;
import com.demo.backend.dto.room.RoomRequest;
import com.demo.backend.dto.room.RoomShortResponse;
import com.demo.backend.dto.room.RoomStudentResponse;
import com.demo.backend.dto.task.TaskResponse;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.Room;
import com.demo.backend.model.Student;

import java.util.List;

public interface RoomService {

    RoomShortResponse createRoom(RoomRequest roomRequest, String id);

    List<RoomShortResponse> getAllByCurrentUser();

    LecturerRoomShortResponse getLecturerListRoom();

    RoomStudentResponse openStudentRoom(String roomId);

    //открытие комнате/задание
    TaskAndAnswerResponse openTask(String roomId, String taskId);

    //открытие комнате/задания/прикрепить файл
    void sendAnswer(String answerTaskId, String documentId);

    RoomLecturerResponse openLecturerRoom(String roomId);

    RoomLecturerResponse addLectureMaterials(String roomId, LectureMaterialRequest lectureMaterialRequest);

    AnswerTaskResponse sendTaskToRefactor(String anwerId, Student student);

    AnswerTaskResponse passTask(String anwerId, Student student, Integer grade);

    TaskResponse createTaskInRoom(TaskRequest taskRequest);

    Room findById(String id);

    Room findByIdAndLecturersContaining(String id, Lecturer lecturer);
}

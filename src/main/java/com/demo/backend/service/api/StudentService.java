package com.demo.backend.service.api;

import com.demo.backend.dto.StudentResponse;
import com.demo.backend.model.Student;
import com.demo.backend.model.User;

import java.util.List;
import java.util.Optional;

public interface StudentService {
    Student getById(String id);

    List<StudentResponse> getAllStudents();

    List<Student> getAll();

    List<Student> findStudentByEmail(List<String> ids);

    List<Student> findByListId(List<String> ids);

    Student getCurrentUserStudent();

    Optional<Student> getByUser(User user);

    Student save(Student studentUser);

}

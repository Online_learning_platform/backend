package com.demo.backend.service.api;

import com.demo.backend.dto.lecturer.LecturerResponse;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.User;

import java.util.List;
import java.util.Optional;


public interface LecturerService {

    Lecturer getById(String id);

    List<LecturerResponse> getLecturerResponse();

    List<Lecturer> getAll();

    List<Lecturer> findLecturerByEmail(List<String> ids);

    Lecturer getCurrentUserLecturer();

    Optional<Lecturer> getByUser(User user);

    Lecturer save(Lecturer lecturer);

}

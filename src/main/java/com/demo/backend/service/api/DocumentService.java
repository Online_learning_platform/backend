package com.demo.backend.service.api;


import com.demo.backend.model.Document;
import com.demo.backend.model.misc.DocumentOrigin;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface DocumentService {

    Document getById(String documentId);

    List<Document> getByIds(List<String> ids);

    // Resource downloadById(String documentId);

    String uploadFile(MultipartFile file, DocumentOrigin origin);

    Document createDocument(String fileName, DocumentOrigin documentOrigin, String hashFileId, long size);

    Document createDocument(String fileName, DocumentOrigin documentOrigin, String hashFileId);
}

package com.demo.backend.service.api;

import org.springframework.web.multipart.MultipartFile;

public interface IPFSService {

    String saveFile(String filePath);

    String createDirectory(String nameDirectory);

    String createDirectoryAndAddFiles(String hashDirectory, MultipartFile multipartFiles);

    String saveFile(MultipartFile file);

    byte[] loadFile(String hash);
}

package com.demo.backend.service.api;

import com.demo.backend.model.AnswerTask;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.Student;
import com.demo.backend.model.Task;

import java.util.List;

public interface AnswerTaskService {
    List<AnswerTask> createAnswerTask(Task task, List<Student> students);

    AnswerTask sendTaskToRefactor(String idTask, Student student, Lecturer lecturer);

    AnswerTask passTask(String idTask, Student student, Integer grade, Lecturer lecturer);

    AnswerTask sendAnswerTask(String id, String documentId);

    AnswerTask findById(String id);

    AnswerTask findByIdAndStudentId(String id, Student student);
}

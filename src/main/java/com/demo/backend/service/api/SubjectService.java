package com.demo.backend.service.api;

import com.demo.backend.dto.SubjectRequest;
import com.demo.backend.dto.SubjectResponse;
import com.demo.backend.model.Subject;

import java.util.List;

public interface SubjectService {
    List<SubjectResponse> getAll();

    List<Subject> findAll();

    SubjectResponse createSubject(SubjectRequest subjectRequest);

    Subject findById(String id);

}

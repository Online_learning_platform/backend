package com.demo.backend.service.api;

import com.demo.backend.dto.TaskRequest;
import com.demo.backend.model.Task;

import java.util.List;

public interface TaskService {
    Task createTask(TaskRequest taskRequest);

    Task findById(String id);

    List<Task> getAllByIds(List<String> ids);

}

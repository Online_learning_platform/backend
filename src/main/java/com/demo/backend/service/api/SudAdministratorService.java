package com.demo.backend.service.api;

import com.demo.backend.dto.StudentRequest;
import com.demo.backend.dto.StudentResponse;
import com.demo.backend.dto.lecturer.LecturerRequest;
import com.demo.backend.dto.lecturer.LecturerResponse;
import com.demo.backend.model.SubAdministrator;
import com.demo.backend.model.User;

import java.util.Optional;

public interface SudAdministratorService {
    Optional<SubAdministrator> getByUser(User user);

    StudentResponse createStudent(StudentRequest studentRequest);

    LecturerResponse createLecturer(LecturerRequest lecturerRequest);

    SubAdministrator getById(String id);
}

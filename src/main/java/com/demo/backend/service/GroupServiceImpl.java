package com.demo.backend.service;

import com.demo.backend.dto.GroupRequest;
import com.demo.backend.dto.GroupResponse;
import com.demo.backend.exception.ResourceNotFoundException;
import com.demo.backend.mapper.GroupMapperService;
import com.demo.backend.model.Group;
import com.demo.backend.model.Student;
import com.demo.backend.repository.GroupRepository;
import com.demo.backend.service.api.GroupService;
import com.demo.backend.service.api.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private StudentService studentService;

    @Autowired
    private GroupMapperService groupMapperService;


    @Override
    public List<GroupResponse> getAllGroupResponse() {
        return groupMapperService.toGroupResponse(findAll());
    }

    @Override
    public GroupResponse createGroup(GroupRequest groupRequest) {
        //List<Student> students = studentService.findByListId(groupRequest.getStudentsIds());
        List<Student> students = studentService.findStudentByEmail(groupRequest.getEmails());

        Group group = new Group();
        group.setName(groupRequest.getName());
        group.setStudents(students);


        Group saveGroup = groupRepository.save(group);
        log.info("Group with id '{}' successfully created", saveGroup.getId());

        return groupMapperService.toGroupResponse(group);
    }

    @Override
    public Group findByStudentListContaining(Student student) {
        return groupRepository.findByStudentsContaining(student)
                .orElseThrow(() -> new ResourceNotFoundException("Student not found in Groups"));
    }

    @Override
    public List<Group> findAll() {
        return groupRepository.findAll();
    }

    @Override
    public Group findById(String id) {
        return groupRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Groups not found"));
    }

    @Override
    public List<Group> findByListId(List<String> ids) {
        return groupRepository.findByIdIn(ids)
                .orElseThrow(() -> new ResourceNotFoundException("Groups not found"));
    }

    @Override
    public Group findByStudent(Student student) {
        return groupRepository.findByStudents(student)
                .orElseThrow(() -> new ResourceNotFoundException("Groups not found by student id '%s'", student.getId()));
    }


}

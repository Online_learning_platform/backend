package com.demo.backend.service;

import com.demo.backend.dto.AnswerTaskResponse;
import com.demo.backend.dto.TaskAndAnswerResponse;
import com.demo.backend.dto.TaskRequest;
import com.demo.backend.dto.lecturer.LecturerRoomShortResponse;
import com.demo.backend.dto.lecturer.LecturerSubjectResponse;
import com.demo.backend.dto.room.LectureMaterialRequest;
import com.demo.backend.dto.room.RoomLecturerResponse;
import com.demo.backend.dto.room.RoomRequest;
import com.demo.backend.dto.room.RoomShortResponse;
import com.demo.backend.dto.room.RoomStudentResponse;
import com.demo.backend.dto.task.TaskResponse;
import com.demo.backend.exception.ResourceNotFoundException;
import com.demo.backend.mapper.RoomMapperService;
import com.demo.backend.mapper.TaskMapperService;
import com.demo.backend.mapper.mapstruct.AnswerTaskMapper;
import com.demo.backend.model.AnswerTask;
import com.demo.backend.model.Document;
import com.demo.backend.model.Group;
import com.demo.backend.model.LectureMaterial;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.Room;
import com.demo.backend.model.Student;
import com.demo.backend.model.Subject;
import com.demo.backend.model.Task;
import com.demo.backend.model.User;
import com.demo.backend.model.misc.Role;
import com.demo.backend.repository.RoomRepository;
import com.demo.backend.service.api.AnswerTaskService;
import com.demo.backend.service.api.DocumentService;
import com.demo.backend.service.api.GroupService;
import com.demo.backend.service.api.LecturerService;
import com.demo.backend.service.api.RoomService;
import com.demo.backend.service.api.StudentService;
import com.demo.backend.service.api.SubjectService;
import com.demo.backend.service.api.TaskService;
import com.demo.backend.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private RoomMapperService roomMapperService;

    @Autowired
    private LecturerService lecturerService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private AnswerTaskMapper answerTaskMapper;

    @Autowired
    private GroupService groupService;

    @Autowired
    private SubjectService subjectService;
    @Autowired
    private UserService userService;

    @Autowired
    private TaskMapperService taskMapperService;

    @Autowired
    private AnswerTaskService answerTaskService;

    @Autowired
    private DocumentService documentService;


    @Override
    public RoomShortResponse createRoom(RoomRequest roomRequest, String id) {
        List<Lecturer> Lecturers = lecturerService.findLecturerByEmail(roomRequest.getLecturerEmails());
        List<Group> byListId = groupService.findByListId(roomRequest.getGroupIds());

        Subject subject = subjectService.findById(id);

        Room room = new Room();
        room.setLecturers(Lecturers);
        room.setGroups(byListId);
        room.setSubject(subject);
        Room saveRoom = roomRepository.save(room);

        RoomShortResponse roomResponse = new RoomShortResponse(saveRoom.getId(), saveRoom.getName());
        //room.setLecturers(); //Todo Response

        return roomResponse;

    }

    @Override
    public List<RoomShortResponse> getAllByCurrentUser() {
        Role role = userService.getCurrentUserContextRole();
        switch (role) {
            case ROLE_STUDENT -> {
                Student student = studentService.getCurrentUserStudent();
                Group studentGroup = groupService.findByStudent(student);
                List<Room> rooms = roomRepository.findAllByGroupsContaining(studentGroup);

                return roomMapperService.toRoomResponse(rooms);
            }
            case ROLE_LECTURER -> {
                Lecturer lecturer = lecturerService.getCurrentUserLecturer();
                List<Room> rooms = roomRepository.findAllByLecturersContaining(lecturer);

                return roomMapperService.toRoomResponse(rooms);
            }
            case ROLE_SUB_ADMINISTRATOR -> {
                List<Room> rooms = roomRepository.findAll();
                return roomMapperService.toRoomResponse(rooms);
            }
            default -> throw new AccessDeniedException(String.format("You can not access this source with role = '%s'", role));
        }
    }


    @Override
    public LecturerRoomShortResponse getLecturerListRoom() {
        Role role = userService.getCurrentUserContextRole();
        Lecturer lecturer = lecturerService.getCurrentUserLecturer();
        List<Room> rooms = roomRepository.findAllByLecturersContaining(lecturer);

        return groupRoomsBySubjects(rooms);
    }


    @Override
    public RoomStudentResponse openStudentRoom(String roomId) {
        User currentUser = userService.getCurrentUser();
        //Role role = userService.getCurrentUserContextRole();
        if (!currentUser.getRoles().contains(Role.ROLE_STUDENT)) {
            throw new AccessDeniedException(String.format("You can not access this source with role = '%s'", currentUser.getRoles()));
        }

        Room room = findById(roomId);

        Student student = studentService.getCurrentUserStudent();
        Group studentGroup = groupService.findByStudentListContaining(student);

        List<String> ids = room.getListTasks().get(studentGroup.getId());
        List<Task> tasks = new ArrayList<>();
        ids.forEach((id) -> {
            Task task = taskService.findById(id);
            tasks.add(task);
        });

        List<TaskResponse> taskResponses = taskMapperService.ToTaskResponse(tasks);


        RoomStudentResponse roomStudentResponse = roomMapperService.toRoomStudentResponse(room);
        //Видит список заданий по его группе listTasks
        roomStudentResponse.setListTasks(taskResponses);

        return roomStudentResponse;
    }

    //открытие комнате/задание
    @Override
    public TaskAndAnswerResponse openTask(String roomId, String taskId) {
        User currentUser = userService.getCurrentUser();
        Role role = userService.getCurrentUserContextRole();
        if (role != Role.ROLE_STUDENT) {
            throw new AccessDeniedException(String.format("You can not access this source with role = '%s'", role));
        }

        Student student = studentService.getCurrentUserStudent();

        Room room = findById(roomId);
        Map<String, List<String>> stringListMap = room.getListTasksAndAnswers().get(student.getId());

        String answerTaskId = stringListMap.get(taskId).stream()
                .filter(element -> student.getId().equals(element))
                .findAny()
                .orElse(null);

        AnswerTask answerTask = answerTaskService.findById(answerTaskId);
        Task task = taskService.findById(taskId);

        return answerTaskMapper.toTaskAndAnswerResponse(task, answerTask);
    }


    //открытие комнате/задания/прикрепить файл
    @Override
    public void sendAnswer(String answerTaskId, String documentId) {
        User currentUser = userService.getCurrentUser();
        Role role = userService.getCurrentUserContextRole();
        if (role != Role.ROLE_STUDENT) {
            throw new AccessDeniedException(String.format("You can not access this source with role = '%s'", role));
        }

        AnswerTask answerTask = answerTaskService.sendAnswerTask(answerTaskId, documentId);

    }

    //Todo
    @Override
    public RoomLecturerResponse openLecturerRoom(String roomId) {
        User currentUser = userService.getCurrentUser();
        Role role = userService.getCurrentUserContextRole();
        if (role != Role.ROLE_LECTURER) {
            throw new AccessDeniedException(String.format("You can not access this source with role = '%s'", role));
        }

        Lecturer lecturer = lecturerService.getCurrentUserLecturer();

        Room room = findByIdAndLecturersContaining(roomId, lecturer);

        RoomLecturerResponse roomLecturerResponse = roomMapperService.toRoomLecturerResponse(room);

        return roomLecturerResponse;
    }


    @Override
    public RoomLecturerResponse addLectureMaterials(String roomId, LectureMaterialRequest lectureMaterialRequest) {

        Lecturer lecturer = lecturerService.getCurrentUserLecturer();

        Role role = userService.getCurrentUserContextRole();
        if (role != Role.ROLE_LECTURER) {
            throw new AccessDeniedException(String.format("You can not access this source with role = '%s'", role));
        }

        Room room = findById(roomId);
        if (!room.getLecturers().contains(lecturer)) {
            throw new AccessDeniedException(String.format("You can not access this source with role = '%s'", role));
        }
        List<LectureMaterial> lectureMaterials = room.getLectureMaterials();

        LectureMaterial lectureMaterial = new LectureMaterial();
        lectureMaterial.setName(lectureMaterialRequest.getName());
        lectureMaterial.setDescription(lectureMaterialRequest.getDescription());

        List<String> documentIds = lectureMaterialRequest.getDocumentIds();
        List<Document> documents = documentService.getByIds(documentIds);
        lectureMaterial.setLectureDocuments(documents);
        lectureMaterials.add(lectureMaterial);

        Room saveRoom = roomRepository.save(room);

        return roomMapperService.toRoomLecturerResponse(saveRoom);
    }

    @Override
    public AnswerTaskResponse sendTaskToRefactor(String anwerId, Student student) {

        Lecturer lecturer = lecturerService.getCurrentUserLecturer();

        Role role = userService.getCurrentUserContextRole();
        if (role != Role.ROLE_LECTURER) {
            throw new AccessDeniedException(String.format("You can not access this source with role = '%s'", role));
        }

        AnswerTask answerTask = answerTaskService.sendTaskToRefactor(anwerId, student, lecturer);

        return answerTaskMapper.toAnswerTaskResponse(answerTask);
    }

    @Override
    public AnswerTaskResponse passTask(String anwerId, Student student, Integer grade) {

        Lecturer lecturer = lecturerService.getCurrentUserLecturer();

        Role role = userService.getCurrentUserContextRole();
        if (role != Role.ROLE_LECTURER) {
            throw new AccessDeniedException(String.format("You can not access this source with role = '%s'", role));
        }

        AnswerTask answerTask = answerTaskService.passTask(anwerId, student, grade, lecturer);

        return answerTaskMapper.toAnswerTaskResponse(answerTask);
    }

    @Override
    public TaskResponse createTaskInRoom(TaskRequest taskRequest) {
        Task task = taskService.createTask(taskRequest);

        List<String> groupIds = taskRequest.getGroups();
        List<Group> groups = new ArrayList<>();

        groupIds.forEach((groupId) -> {
            Group group = groupService.findById(groupId);
            groups.add(group);
        });

        Room room = findById(taskRequest.getRoomId());
        Map<String, List<String>> listTasks = room.getListTasks();
        Map<String, Map<String, List<String>>> listTasksAndAnswers = room.getListTasksAndAnswers();

        groups.forEach(group -> {
            String taskid = task.getId();

            List<String> taskIds = listTasks.get(group.getId());
            taskIds.add(taskid);
            Map<String, List<String>> stringListMap = listTasksAndAnswers.get(group.getId());
            List<AnswerTask> answerTasks = answerTaskService.createAnswerTask(task, group.getStudents());

            List<String> answerTaskIds = new ArrayList<>();

            answerTasks.forEach((answerTask) -> {
                answerTaskIds.add(answerTask.getId());
            });

            stringListMap.put(taskid, answerTaskIds);
        });

        return taskMapperService.ToTaskResponse(task);
    }

    private LecturerRoomShortResponse groupRoomsBySubjects(List<Room> rooms) {

        Subject subject1 = rooms.get(0).getSubject();

        LecturerRoomShortResponse lecturerRoomShortResponse = new LecturerRoomShortResponse();
        List<LecturerSubjectResponse> subjectResponse = lecturerRoomShortResponse.getSubjectlist();
        subjectResponse.add(new LecturerSubjectResponse(subject1.getId(), subject1.getName(), subject1.getDescription(), new ArrayList<RoomShortResponse>()));


        rooms.forEach(room -> {
            Subject subject = room.getSubject();

            subjectResponse.forEach(name -> {

                if (subject.getName().equals(name.getName())) {
                    RoomShortResponse roomShortResponse = new RoomShortResponse(room.getId(), room.getName());
                    name.getRooms().add(roomShortResponse);
                } else {
                    List<RoomShortResponse> list = new ArrayList<RoomShortResponse>();
                    list.add(new RoomShortResponse(room.getId(), room.getName()));
                    subjectResponse.add(new LecturerSubjectResponse(subject1.getId(), subject1.getName(), subject1.getDescription(), list));
                }
            });
        });

        return lecturerRoomShortResponse;
    }

    @Override
    public Room findById(String id) {
        return roomRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Room not found"));
    }


    @Override
    public Room findByIdAndLecturersContaining(String id, Lecturer lecturer) {
        return roomRepository.findByIdAndLecturersContaining(id, lecturer)
                .orElseThrow(() -> new ResourceNotFoundException("Room not found"));
    }

}

package com.demo.backend.repository;

import com.demo.backend.model.AnswerTask;
import com.demo.backend.model.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AnswerTaskRepository extends MongoRepository<AnswerTask, String> {

    Optional<AnswerTask> findByIdAndStudentId(String id, Student student);

}

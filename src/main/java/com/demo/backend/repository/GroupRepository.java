package com.demo.backend.repository;

import com.demo.backend.model.Group;
import com.demo.backend.model.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupRepository extends MongoRepository<Group, String> {

    Optional<List<Group>> findByIdIn(List<String> ids);

    Optional<Group> findByStudentsContaining(Student student);

    Optional<Group> findByStudents(Student student);
}

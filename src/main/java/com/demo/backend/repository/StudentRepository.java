package com.demo.backend.repository;

import com.demo.backend.model.Student;
import com.demo.backend.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends MongoRepository<Student, String> {
    Optional<Student> findByUser(User user);

    Optional<List<Student>> findByIdIn(List<String> ids);

    Optional<List<Student>> findByUserIn(List<User> users);
}

package com.demo.backend.repository;

import com.demo.backend.model.Administrator;
import com.demo.backend.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdministratorRepository extends MongoRepository<Administrator, String> {

    Optional<Administrator> findByUser(User user);

}

package com.demo.backend.repository;

import com.demo.backend.model.Document;
import com.demo.backend.model.misc.DocumentOrigin;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DocumentRepository extends MongoRepository<Document, String> {

    List<Document> findAllByIdIn(List<String> ids);

    Optional<Document> findByIdAndDocumentOrigin(String id, DocumentOrigin documentOrigin);

}

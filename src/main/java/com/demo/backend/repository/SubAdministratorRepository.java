package com.demo.backend.repository;

import com.demo.backend.model.SubAdministrator;
import com.demo.backend.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SubAdministratorRepository extends MongoRepository<SubAdministrator, String> {

    Optional<SubAdministrator> findByUser(User user);

}

package com.demo.backend.repository;

import com.demo.backend.model.Group;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.Room;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomRepository extends MongoRepository<Room, String> {

    List<Room> findAllByLecturersContaining(Lecturer lecturer);

    Optional<Room> findByIdAndLecturersContaining(String id, Lecturer lecturer);

    List<Room> findAllByGroupsContaining(Group group);


}

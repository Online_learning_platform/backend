package com.demo.backend.mapper;

import com.demo.backend.dto.SubjectResponse;
import com.demo.backend.model.Subject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubjectMapperService {


    public List<SubjectResponse> toSubjectResponse(List<Subject> subjects) {
        List<SubjectResponse> subjectResponses = new ArrayList<>();

        subjects.forEach(subject -> subjectResponses.add(toSubjectResponse(subject)));
        return subjectResponses;
    }

    public SubjectResponse toSubjectResponse(Subject subject) {
        SubjectResponse subjectResponse = new SubjectResponse(subject.getId(), subject.getName(), subject.getDescription());
        return subjectResponse;
    }

}

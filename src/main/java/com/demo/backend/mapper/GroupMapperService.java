package com.demo.backend.mapper;

import com.demo.backend.dto.GroupResponse;
import com.demo.backend.dto.StudentShortResponse;
import com.demo.backend.model.Group;
import com.demo.backend.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupMapperService {

    @Autowired
    private StudentMapperService studentMapperService;

    public List<GroupResponse> toGroupResponse(List<Group> groups) {
        List<GroupResponse> newGroups = new ArrayList<>();
        groups.forEach(group -> newGroups.add(toGroupResponse(group)));

        return newGroups;
    }

    public GroupResponse toGroupResponse(Group group) {
        List<Student> students = group.getStudents();
        List<StudentShortResponse> studentShortResponses = new ArrayList<>();
        students.forEach(student -> studentShortResponses.add(studentMapperService.toStudentShortResponse(student)));

        GroupResponse newGroup = new GroupResponse();
        newGroup.setId(group.getId());
        newGroup.setName(group.getName());
        newGroup.setStudents(studentShortResponses);

        return newGroup;
    }


}

package com.demo.backend.mapper;

import com.demo.backend.dto.TaskRequest;
import com.demo.backend.dto.task.TaskResponse;
import com.demo.backend.mapper.mapstruct.TaskMapper;
import com.demo.backend.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskMapperService {

    @Autowired
    private TaskMapper taskMapper;

    public Task fromTaskRequest(TaskRequest taskRequest) {
        return taskMapper.fromTaskRequest(taskRequest);
    }

    public TaskResponse ToTaskResponse(Task task) {
        return taskMapper.ToTaskResponse(task);
    }

    public List<TaskResponse> ToTaskResponse(List<Task> task) {
        return taskMapper.ToTaskResponse(task);
    }

}

package com.demo.backend.mapper;

import com.demo.backend.dto.lecturer.LecturerResponse;
import com.demo.backend.mapper.mapstruct.LecturerMapper;
import com.demo.backend.model.Lecturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LecturerMapperService {

    @Autowired
    private LecturerMapper lecturerMapper;

    public List<LecturerResponse> toListLecturerResponse(List<Lecturer> lecturers) {
        return lecturerMapper.toListLecturerResponse(lecturers);
    }

}

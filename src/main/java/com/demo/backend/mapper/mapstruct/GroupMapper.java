package com.demo.backend.mapper.mapstruct;

import com.demo.backend.dto.GroupResponse;
import com.demo.backend.model.Group;
import org.mapstruct.Mapper;

@Mapper
public interface GroupMapper {


    GroupResponse toGroupResponse(Group group);

}

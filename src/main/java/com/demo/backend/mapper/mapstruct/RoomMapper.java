package com.demo.backend.mapper.mapstruct;

import com.demo.backend.dto.DocumentResponse;
import com.demo.backend.dto.lecturer.LecturerShortResponse;
import com.demo.backend.dto.room.RoomLecturerResponse;
import com.demo.backend.dto.room.RoomShortResponse;
import com.demo.backend.dto.room.RoomStudentResponse;
import com.demo.backend.model.Document;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.Room;
import com.demo.backend.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper
public interface RoomMapper {

    String USER_NAME_MAPPING_PATTERN = "%s %s";


    @Mapping(source = "user", target = "name", qualifiedByName = "mapUserName")
    @Mapping(source = "user.email", target = "email")
    LecturerShortResponse toLecturerShortResponse(Lecturer lecturer);

    @Mapping(source = "subject.name", target = "name")
    RoomShortResponse toRoomResponse(Room room);

    List<RoomShortResponse> toRoomResponse(List<Room> room);

    @Mapping(target = "listTasks", ignore = true)
    @Mapping(source = "subject.name", target = "name")
    @Mapping(source = "subject.description", target = "description")
    RoomStudentResponse toRoomStudentResponse(Room room);

    @Mapping(source = "subject.name", target = "name")
    @Mapping(source = "subject.description", target = "description")
    @Mapping(target = "listTasks", ignore = true)
    RoomLecturerResponse toRoomLecturerResponse(Room room);

    DocumentResponse documentToDocumentResponse(Document document);

    List<DocumentResponse> documentToDocumentResponse(List<Document> document);

    @Named("mapUserName")
    default String mapUserName(User user) {
        return String.format(USER_NAME_MAPPING_PATTERN, user.getFirstName(), user.getLastName());
    }
}

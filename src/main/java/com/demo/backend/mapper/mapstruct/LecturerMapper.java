package com.demo.backend.mapper.mapstruct;

import com.demo.backend.dto.lecturer.LecturerResponse;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper
public interface LecturerMapper {

    String USER_NAME_MAPPING_PATTERN = "%s %s";

    @Mapping(source = "user.phoneNumber", target = "phone.number")
    @Mapping(source = "user.dialCode", target = "phone.dialCode")
    @Mapping(source = "user.email", target = "email")
    @Mapping(source = "user.birthDate", target = "birthDate")
    @Mapping(source = "user", target = "name", qualifiedByName = "mapUserName")
    LecturerResponse toLecturerResponse(Lecturer lecturer);

    List<LecturerResponse> toListLecturerResponse(List<Lecturer> lecturers);

    @Named("mapUserName")
    default String mapUserName(User user) {
        return String.format(USER_NAME_MAPPING_PATTERN, user.getFirstName(), user.getLastName());
    }

}

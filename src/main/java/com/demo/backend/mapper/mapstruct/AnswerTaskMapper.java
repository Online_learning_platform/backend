package com.demo.backend.mapper.mapstruct;

import com.demo.backend.dto.AnswerTaskResponse;
import com.demo.backend.dto.TaskAndAnswerResponse;
import com.demo.backend.model.AnswerTask;
import com.demo.backend.model.Task;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface AnswerTaskMapper {

    AnswerTaskResponse toAnswerTaskResponse(AnswerTask answerTask);

    @Mapping(source = "task.id", target = "taskId")
    @Mapping(source = "task.name", target = "name")
    @Mapping(source = "task.description", target = "description")
    @Mapping(source = "task.taskDocuments", target = "taskDocuments")
    @Mapping(source = "answerTask.id", target = "answerTaskId")
    @Mapping(source = "answerTask.answerTaskDocuments", target = "answerTaskDocuments")
    @Mapping(source = "answerTask.taskStatus", target = "taskStatus")
    @Mapping(source = "answerTask.lecturerId", target = "lecturer")
    TaskAndAnswerResponse toTaskAndAnswerResponse(Task task, AnswerTask answerTask);

}

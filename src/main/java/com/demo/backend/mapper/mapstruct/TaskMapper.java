package com.demo.backend.mapper.mapstruct;

import com.demo.backend.dto.TaskRequest;
import com.demo.backend.dto.task.TaskResponse;
import com.demo.backend.model.Task;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface TaskMapper {

    TaskResponse ToTaskResponse(Task task);

    List<TaskResponse> ToTaskResponse(List<Task> task);
    
    Task fromTaskRequest(TaskRequest taskRequest);


}

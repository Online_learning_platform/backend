package com.demo.backend.mapper.mapstruct;

import com.demo.backend.dto.SubAdministratorRequest;
import com.demo.backend.dto.lecturer.LecturerRequest;
import com.demo.backend.dto.lecturer.LecturerResponse;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.SubAdministrator;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface SubAdministratorMapper {


    @Mapping(source = "email", target = "user.email")
    @Mapping(source = "firstName", target = "user.firstName")
    @Mapping(source = "lastName", target = "user.lastName")
    @Mapping(source = "phone.dialCode", target = "user.dialCode")
    @Mapping(source = "phone.number", target = "user.phoneNumber")
    SubAdministrator toModel(SubAdministratorRequest subAdministratorRequest);

    @Mapping(source = "email", target = "user.email")
    @Mapping(source = "firstName", target = "user.firstName")
    @Mapping(source = "lastName", target = "user.lastName")
    @Mapping(source = "phone.dialCode", target = "user.dialCode")
    @Mapping(source = "phone.number", target = "user.phoneNumber")
    Lecturer toModelLecturer(LecturerRequest lecturerRequest);

    @Mapping(source = "user.phoneNumber", target = "phone.number")
    @Mapping(source = "user.dialCode", target = "phone.dialCode")
    @Mapping(source = "user.email", target = "email")
    @Mapping(source = "user.birthDate", target = "birthDate")
    LecturerResponse toLecturerResponse(Lecturer lecturer);

}

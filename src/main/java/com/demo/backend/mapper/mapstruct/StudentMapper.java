package com.demo.backend.mapper.mapstruct;

import com.demo.backend.dto.StudentRequest;
import com.demo.backend.dto.StudentResponse;
import com.demo.backend.dto.StudentShortResponse;
import com.demo.backend.model.Student;
import com.demo.backend.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper
public interface StudentMapper {

    String USER_NAME_MAPPING_PATTERN = "%s %s";

    @Mapping(source = "email", target = "user.email")
    @Mapping(source = "firstName", target = "user.firstName")
    @Mapping(source = "lastName", target = "user.lastName")
    @Mapping(source = "phone.dialCode", target = "user.dialCode")
    @Mapping(source = "phone.number", target = "user.phoneNumber")
    @Mapping(source = "birthDate", target = "user.birthDate")
    Student toModel(StudentRequest studentRequest);

    @Mapping(source = "user.phoneNumber", target = "phone.number")
    @Mapping(source = "user.dialCode", target = "phone.dialCode")
    @Mapping(source = "user.email", target = "email")
    @Mapping(source = "user.birthDate", target = "birthDate")
    @Mapping(source = "user", target = "name", qualifiedByName = "mapUserName")
    StudentResponse toStudentResponse(Student student);

    @Mapping(source = "user.email", target = "email")
    @Mapping(source = "user", target = "name", qualifiedByName = "mapUserName")
    StudentShortResponse toStudentShortResponse(Student student);
    
    List<StudentResponse> toStudentsResponse(List<Student> student);

    @Named("mapUserName")
    default String mapUserName(User user) {
        return String.format(USER_NAME_MAPPING_PATTERN, user.getFirstName(), user.getLastName());
    }

}

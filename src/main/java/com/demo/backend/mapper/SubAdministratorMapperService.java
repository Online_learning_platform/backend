package com.demo.backend.mapper;

import com.demo.backend.dto.SubAdministratorRequest;
import com.demo.backend.dto.lecturer.LecturerRequest;
import com.demo.backend.dto.lecturer.LecturerResponse;
import com.demo.backend.mapper.mapstruct.SubAdministratorMapper;
import com.demo.backend.mapper.mixin.NameMixin;
import com.demo.backend.model.Lecturer;
import com.demo.backend.model.SubAdministrator;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SubAdministratorMapperService implements NameMixin {

    @Autowired
    private SubAdministratorMapper subAdministratorMapper;

    public SubAdministrator toModel(SubAdministratorRequest subAdministratorRequest) {
        return subAdministratorMapper.toModel(subAdministratorRequest);
    }

    public Lecturer toModelLecturer(LecturerRequest lecturerRequest) {
        return subAdministratorMapper.toModelLecturer(lecturerRequest);
    }

    public LecturerResponse toLecturerResponse(Lecturer lecturer) {
        LecturerResponse lecturerResponse = subAdministratorMapper.toLecturerResponse(lecturer);
        lecturerResponse.setName(mapUserName(lecturer.getUser()));

        return lecturerResponse;
    }
}

package com.demo.backend.mapper;

import com.demo.backend.dto.StudentRequest;
import com.demo.backend.dto.StudentResponse;
import com.demo.backend.dto.StudentShortResponse;
import com.demo.backend.mapper.mapstruct.StudentMapper;
import com.demo.backend.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentMapperService {

    @Autowired
    private StudentMapper studentMapper;

    public Student toModel(StudentRequest studentRequest) {
        return studentMapper.toModel(studentRequest);
    }

    public StudentResponse toStudentResponse(Student student) {
        return studentMapper.toStudentResponse(student);
    }

    public List<StudentResponse> toStudentsResponse(List<Student> students) {
        return studentMapper.toStudentsResponse(students);
    }

    public StudentShortResponse toStudentShortResponse(Student student) {
        return studentMapper.toStudentShortResponse(student);
    }

}

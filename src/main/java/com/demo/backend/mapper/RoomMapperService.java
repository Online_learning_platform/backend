package com.demo.backend.mapper;

import com.demo.backend.dto.DocumentResponse;
import com.demo.backend.dto.room.RoomLecturerResponse;
import com.demo.backend.dto.room.RoomShortResponse;
import com.demo.backend.dto.room.RoomStudentResponse;
import com.demo.backend.dto.task.TaskResponse;
import com.demo.backend.mapper.mapstruct.RoomMapper;
import com.demo.backend.model.Document;
import com.demo.backend.model.Room;
import com.demo.backend.model.Task;
import com.demo.backend.service.api.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RoomMapperService {

    @Autowired
    private RoomMapper roomMapper;

    @Autowired
    private TaskMapperService taskMapperService;

    @Autowired
    private TaskService taskService;


    public RoomShortResponse toRoomResponse(Room room) {
        return roomMapper.toRoomResponse(room);
    }

    public List<RoomShortResponse> toRoomResponse(List<Room> rooms) {
        return roomMapper.toRoomResponse(rooms);
    }

    public RoomStudentResponse toRoomStudentResponse(Room room) {
        return roomMapper.toRoomStudentResponse(room);
    }


    public RoomLecturerResponse toRoomLecturerResponse(Room room) {
        RoomLecturerResponse roomLecturerResponse = roomMapper.toRoomLecturerResponse(room);

        Map<String, List<String>> listTasks = room.getListTasks();

        Map<String, List<TaskResponse>> newTask = new HashMap<>();

        listTasks.forEach((id, listTask) -> {
            List<Task> allByIds = taskService.getAllByIds(listTask);
            newTask.put(id, taskMapperService.ToTaskResponse(allByIds));
        });

        roomLecturerResponse.setListTasks(newTask);

        return roomLecturerResponse;
    }

    public DocumentResponse documentToDocumentResponse(Document document) {
        return roomMapper.documentToDocumentResponse(document);
    }

    public List<DocumentResponse> documentToDocumentResponse(List<Document> document) {
        return roomMapper.documentToDocumentResponse(document);
    }
}

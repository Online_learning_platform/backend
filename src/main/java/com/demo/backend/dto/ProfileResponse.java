package com.demo.backend.dto;

import com.demo.backend.model.misc.Role;
import lombok.Data;

import java.util.List;

@Data
public class ProfileResponse {

    private String id;
    private String name;
    //private String address;
    private String email;
    private PhoneNumberInfo phoneNumber;

    private List<Role> roles;
    //private Locale language;

}

package com.demo.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SubjectResponse {
    private String id;
    private String name;
    private String description;
}

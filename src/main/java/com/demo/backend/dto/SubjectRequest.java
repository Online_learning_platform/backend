package com.demo.backend.dto;

import lombok.Data;

@Data
public class SubjectRequest {
    private String id;
    private String name;
    private String description;
}

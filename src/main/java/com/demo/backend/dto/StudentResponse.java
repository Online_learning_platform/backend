package com.demo.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class StudentResponse {
    private String id;
    private String email;
    private String name;
    private PhoneNumberInfo phone;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
}

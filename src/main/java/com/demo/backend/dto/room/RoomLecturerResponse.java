package com.demo.backend.dto.room;

import com.demo.backend.dto.GroupResponse;
import com.demo.backend.dto.LectureMaterialResponse;
import com.demo.backend.dto.task.TaskResponse;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class RoomLecturerResponse {
    private String id;
    private String name;
    private String description;
    private List<GroupResponse> groups;
    private Map<String, List<TaskResponse>> listTasks;
    private List<LectureMaterialResponse> lectureMaterials;
}

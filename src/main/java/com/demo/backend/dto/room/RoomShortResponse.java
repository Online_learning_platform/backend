package com.demo.backend.dto.room;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RoomShortResponse {
    private String id;
    private String name;
}

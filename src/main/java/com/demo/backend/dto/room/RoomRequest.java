package com.demo.backend.dto.room;

import lombok.Data;

import java.util.List;

@Data
public class RoomRequest {
    private List<String> lecturerEmails;
    private List<String> groupIds;
}

package com.demo.backend.dto.room;

import com.demo.backend.dto.GroupResponse;
import com.demo.backend.dto.lecturer.LecturerShortResponse;
import lombok.Data;

import java.util.List;

@Data
public class RoomResponse {
    private String id;
    private String name;
    private List<LecturerShortResponse> lecturers;
    private List<GroupResponse> groups;

    
}

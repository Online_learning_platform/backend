package com.demo.backend.dto.room;

import com.demo.backend.dto.LectureMaterialResponse;
import com.demo.backend.dto.lecturer.LecturerShortResponse;
import com.demo.backend.dto.task.TaskResponse;
import lombok.Data;

import java.util.List;

@Data
public class RoomStudentResponse {
    private String id;
    private String name;
    private String description;
    private List<LecturerShortResponse> lecturers;
    private List<TaskResponse> listTasks;
    private List<LectureMaterialResponse> lectureMaterials;
}

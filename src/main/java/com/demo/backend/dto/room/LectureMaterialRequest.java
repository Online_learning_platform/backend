package com.demo.backend.dto.room;

import lombok.Data;

import java.util.List;

@Data
public class LectureMaterialRequest {
    private String name;
    private String description;
    private List<String> documentIds;
}

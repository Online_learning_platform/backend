package com.demo.backend.dto;

import lombok.Data;

import java.util.List;

@Data
public class GroupRequest {
    private String name;
    private List<String> emails;
}

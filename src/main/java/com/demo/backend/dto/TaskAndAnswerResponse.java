package com.demo.backend.dto;

import com.demo.backend.dto.lecturer.LecturerResponse;
import com.demo.backend.model.misc.TaskStatus;
import lombok.Data;

import java.util.List;

@Data
public class TaskAndAnswerResponse {
    private String taskId;
    private String name;
    private String description;
    private String answerTaskId;
    private List<DocumentResponse> taskDocuments;
    private List<DocumentResponse> answerTaskDocuments;
    private TaskStatus taskStatus;
    private LecturerResponse lecturer;
}

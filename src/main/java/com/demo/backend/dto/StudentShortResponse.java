package com.demo.backend.dto;

import lombok.Data;

@Data
public class StudentShortResponse {
    private String id;
    private String email;
    private String name;
}

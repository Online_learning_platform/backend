package com.demo.backend.dto.lecturer;

import com.demo.backend.dto.room.RoomShortResponse;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class LecturerSubjectResponse {
    private String id;
    private String name;
    private String description;
    private List<RoomShortResponse> rooms = new ArrayList<RoomShortResponse>();
}

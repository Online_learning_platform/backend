package com.demo.backend.dto.lecturer;

import com.demo.backend.dto.PhoneNumberInfo;
import lombok.Data;

@Data
public class LecturerRequest {
    private String email;
    private String firstName;
    private String lastName;
    private PhoneNumberInfo phone;
    private String nameInstitution;
}

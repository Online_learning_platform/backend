package com.demo.backend.dto.lecturer;

import com.demo.backend.dto.PhoneNumberInfo;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class LecturerResponse {
    private String id;
    private String email;
    private String name;
    private PhoneNumberInfo phone;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDate birthDate;

    private String nameInstitution;
}

package com.demo.backend.dto.lecturer;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class LecturerRoomShortResponse {
    private List<LecturerSubjectResponse> subjectlist = new ArrayList<LecturerSubjectResponse>();
}

package com.demo.backend.dto.lecturer;

import lombok.Data;

@Data
public class LecturerShortResponse {
    private String id;
    private String email;
    private String name;
}

package com.demo.backend.dto;

import lombok.Data;

import java.util.List;

@Data
public class LectureMaterialResponse {
    private String name;
    private String description;
    private List<DocumentResponse> lectureDocuments;
}

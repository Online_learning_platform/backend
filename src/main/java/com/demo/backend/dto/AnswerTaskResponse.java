package com.demo.backend.dto;

import com.demo.backend.dto.lecturer.LecturerResponse;
import com.demo.backend.model.misc.TaskStatus;
import lombok.Data;

import java.util.List;

@Data
public class AnswerTaskResponse {
    private String id;
    private List<DocumentResponse> answerTaskDocuments;
    private TaskStatus taskStatus = TaskStatus.STATUS_NOT_STARTED;
    private LecturerResponse lecturerId;

}

package com.demo.backend.dto;

import lombok.Data;

import java.util.List;

@Data
public class GroupResponse {
    private String id;
    private String name;
    private List<StudentShortResponse> students;
}

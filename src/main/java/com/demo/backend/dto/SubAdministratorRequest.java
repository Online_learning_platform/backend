package com.demo.backend.dto;

import lombok.Data;

@Data
public class SubAdministratorRequest {
    private String email;
    private String firstName;
    private String lastName;
    private String surName;
    private PhoneNumberInfo phone;
}

package com.demo.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class StudentRequest {
    private String email;
    private String firstName;
    private String lastName;
    private String surName;
    private PhoneNumberInfo phone;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDate birthDate;

}

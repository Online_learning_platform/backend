package com.demo.backend.dto;

import com.demo.backend.model.misc.TaskType;
import lombok.Data;

import java.util.List;

@Data
public class TaskRequest {
    private String roomId;
    private String name;
    private TaskType taskType;
    private List<String> groups;
    private String description;
    private List<String> documentIds;
}

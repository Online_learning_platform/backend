package com.demo.backend.dto.task;

import com.demo.backend.dto.DocumentResponse;
import com.demo.backend.model.misc.TaskType;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class TaskResponse {
    private String id;
    private String name;
    private String description;
    private Date dateTo;
    private TaskType taskType;
    private List<DocumentResponse> lectureDocuments;
}

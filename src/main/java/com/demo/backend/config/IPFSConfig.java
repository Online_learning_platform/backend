package com.demo.backend.config;

import io.ipfs.api.IPFS;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;


@Configuration
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class IPFSConfig {

    public IPFS ipfs;

    public IPFSConfig() {

        ///ip4/127.0.0.1/tcp/5001

        // /dnsaddr/ipfs.infura.io/tcp/5001/https
        ipfs = new IPFS("/ip4/127.0.0.1/tcp/5001");
    }

}

package com.demo.backend.model;

import com.demo.backend.model.misc.TaskStatus;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

@Data
@org.springframework.data.mongodb.core.mapping.Document("answerTask")
public class AnswerTask {
    @Id
    private String id;

    @DBRef
    private Student studentId;

    private Integer grade;

    @DBRef
    private List<Document> answerTaskDocuments;

    private TaskStatus taskStatus = TaskStatus.STATUS_NOT_STARTED;

    @DBRef
    private Lecturer lecturerId;


}

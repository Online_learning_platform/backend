package com.demo.backend.model;

import com.demo.backend.model.misc.DocumentOrigin;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@org.springframework.data.mongodb.core.mapping.Document("document")
public class Document {

    @Id
    private String id;

    private String fileName;

    private String hashFileId;

    private DocumentOrigin documentOrigin;

    private long size;

    @CreatedDate
    private LocalDateTime createdAt;
}

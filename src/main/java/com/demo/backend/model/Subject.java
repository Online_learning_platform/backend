package com.demo.backend.model;

import lombok.Data;
import nonapi.io.github.classgraph.json.Id;

@Data
@org.springframework.data.mongodb.core.mapping.Document("subject")
public class Subject {
    @Id
    private String id;

    private String name;
    private String description;

    //@DBRef
    //private List<Room> rooms = new ArrayList<>();//
}

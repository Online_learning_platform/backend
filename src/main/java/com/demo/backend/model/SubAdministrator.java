package com.demo.backend.model;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("subAdministrator")
public class SubAdministrator {

    @Id
    private String id;

    @DBRef
    private User user;

}

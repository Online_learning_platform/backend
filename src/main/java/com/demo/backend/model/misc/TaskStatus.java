package com.demo.backend.model.misc;

public enum TaskStatus {
    STATUS_NOT_STARTED,//не начато
    STATUS_IN_PROGRESS,// не проверено
    STATUS_IN_REFACTOR,
    STATUS_REQUIRED// сдано
}

package com.demo.backend.model.misc;

public enum Role {
    ROLE_STUDENT,
    ROLE_LECTURER,
    ROLE_SUB_ADMINISTRATOR,
    ROLE_ADMINISTRATOR,

}

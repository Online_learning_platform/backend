package com.demo.backend.model.misc;

public enum TaskType {
    TYPE_LAB,
    TYPE_SURVEY
}

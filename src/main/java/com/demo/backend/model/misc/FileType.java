package com.demo.backend.model.misc;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

@Getter
@AllArgsConstructor
public enum FileType {

    PDF("application/pdf"),
    DOC("application/msword"),
    DOCX("application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
    XLS("application/vnd.ms-excel"),
    XLSX("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
    PNG("image/png"),
    JPG("image/jpeg"),
    JPEG("image/jpeg"),
    MP3("audio/mpeg"),
    MP4("video/mp4"),
    MKW("video/x-matroska"),
    MOV("video/quicktime"),
    OTHER(null);

    private String mediaType;

    public static FileType fromFileName(String fileName) {
        return extractExtension(fileName)
                .flatMap(s -> Stream.of(FileType.values())
                        .filter(fileType -> fileType.name().equalsIgnoreCase(s))
                        .findFirst())
                .orElse(OTHER);
    }

    public static Optional<String> extractExtension(String fileName) {
        return Optional.ofNullable(fileName)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(fileName.lastIndexOf(".") + 1));
    }
}

package com.demo.backend.model.misc;

public enum DocumentOrigin {
    LECTURE,
    LECTURE_TASK,
    STUDENT_ANSWER_LAB
}

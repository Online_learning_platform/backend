package com.demo.backend.model;

import lombok.Data;

@Data
public class IpfsDirectory {
    private String name;
    private String hash;
}

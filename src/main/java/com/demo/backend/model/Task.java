package com.demo.backend.model;

import com.demo.backend.model.misc.TaskType;
import lombok.Data;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.Date;
import java.util.List;


@Data
@org.springframework.data.mongodb.core.mapping.Document("task")
public class Task {
    @Id
    private String id;
    private String name;
    private String description;
    private Date dateTo;
    private TaskType taskType;

    @DBRef
    private List<Document> taskDocuments;
}

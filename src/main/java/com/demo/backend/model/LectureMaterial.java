package com.demo.backend.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

@Data
public class LectureMaterial {
    private String name;
    private String description;
    @DBRef
    private List<Document> lectureDocuments;
}

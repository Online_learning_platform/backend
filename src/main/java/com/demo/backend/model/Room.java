package com.demo.backend.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@org.springframework.data.mongodb.core.mapping.Document("room")
public class Room {
    @Id
    private String id;
    private String name;

    @DBRef
    private Subject subject;

    @DBRef
    private List<Lecturer> lecturers;

    @DBRef
    private List<Group> groups;

    //id группы  id задания Task
    private Map<String, List<String>> listTasks;

    //id группы,  id задания , List ответов AnswerTask
    private Map<String, Map<String, List<String>>> listTasksAndAnswers;

    private List<LectureMaterial> lectureMaterials = new ArrayList<>();
    //Описение класс материал
}

package com.demo.backend.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document("administrator")
public class Administrator {

    @Id
    private String id;

    @DBRef
    private User user;

    @DBRef
    private List<SubAdministrator> subAdministrator;

}

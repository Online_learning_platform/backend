package com.demo.backend.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

@Data
@org.springframework.data.mongodb.core.mapping.Document("topic")
public class Topic {
    @Id
    private String id;

    private String name;

    @DBRef
    private Task taskId;

    @DBRef
    private List<Document> document;
}
